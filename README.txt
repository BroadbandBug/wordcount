 _       __                __                        __ 
| |     / /____  _________/ /_________  __  ______  / /_
| | /| / // __ \/ ___/ __  // ___/ __ \/ / / / __ \/ __/
| |/ |/ // /_/ / /  / /_/ // /__/ /_/ / /_/ / / / / /_  
|__/|__/ \____/_/   \__,_/ \___/\____/\__,_/_/ /_/\__/  

Author: Kevin Klug
Class: ECE275a
Date: 10/05/2011
Assignment2

Usage: wordrank inputfile

Description:
This program will list ten most used words in the input file.

Implementation:
wordcount creates a singly linked list and adds an element to this list for every unique word found.
If the word has been used before then an count in the element of the repeated word is incremented.
After the whold file has been scanned, the top ten are printed to stdout in the format of least to most
used words.