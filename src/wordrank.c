/**************************************************************************************************/
/*
 * File: wordrank.h
 * Author: Kevin Klug
 * Date: 29 September 2011
 *
 * Description: Functions for the implementation of ECE275 Assigment 2: Wordrank
 *
 */
/**************************************************************************************************/

#include "global.h"
#include "list.h"
#include "wordrank.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>


//*********************************************
//Populates the a linked list from a given file
//*********************************************
bool populate_list( char *inputFileName, List *list){
    
    FILE *inputFile;
    char raw_buff[BIGGESTWORD];
    char parsed_buff[BIGGESTWORD];
    char blank_string[BIGGESTWORD];
    int count = 0;
    ListElmt *element;
    
    //Open File
    inputFile = fopen(inputFileName, "r");
    
    //Check if file exists
    if(inputFile == NULL){
        printf("Error: Cannot open file.\n");
        return FALSE;
    }
    
    
    //Scan in file string by string
    while((fscanf(inputFile, "%s", raw_buff) != 0) && !feof(inputFile)){
        
        //Remove commas, periods, and convert word to lowercase
        for (count = 0; (raw_buff[count] != ',')&&(raw_buff[count] != '.')&&(count<BIGGESTWORD); count++) {
            parsed_buff[count] = tolower(raw_buff[count]);
        }
        
        element = repeated_word(list, parsed_buff);
        
        //If the word isn't found, add it to the beginning of the list and increment word count
        if(element == NULL){
            if(list_ins_next(list, list->tail, parsed_buff) == FALSE) return FALSE;
            list->tail->word_count++;
        }else{
            //Else increment the wordcount of the repeated word
            element->word_count++;
        }
        //Clear the parsed and blank_strings
        for(count = 0; count < BIGGESTWORD; count++){
            parsed_buff[count] = '\0';
            blank_string[count] = '\0';
        }
    }
    
    //Close file
    fclose(inputFile);
    return TRUE;

}


//**************************************************************************
//Given a list, this function seaches the list elements for the given string
//**************************************************************************
ListElmt *repeated_word( List *list, char *input_word){
    
    ListElmt *element_check;
    
    element_check = list->head;
    
    for (element_check = list->head; element_check != NULL; element_check=element_check->next) {
        if (strcmp(element_check->word, input_word)==0) {
            break;
        }
    }
    return element_check;
    
}
//*****************************************************************************************
//Given a list, this function searches for, and prints, the top num of elements in the list
//*****************************************************************************************

void top_words(List *list, int num){
    
    ListElmt *element;
    ListElmt *top_element;
    
    int biggest = 0;
    int count = 1;
    
    List top_ten;
    list_init(&top_ten);
    
    for(count = 1; count <= num; count++){
        
        //This section deals with finding the next largest uncheck element in the linked list.
        //The search is not the fastest because it has to search the list everytime for the next largest element.
        //I think this method of search is similiar to a bubblesort?...
        for(element=list->head; element != NULL; element=element->next){
            if((biggest <= element->word_count)&& !(element->checked)){
                    biggest = element->word_count;
                top_element = element;
            }
        } 
        
        //If the user wants the count to go from 1-10, this code will do just that.
        //Otherwise, a list top_ten is used to store the words in reverse order
        //and these are printed instead.
        //printf("%d %s (%d)\n", count, top_element->word, top_element->word_count);
        if((top_element != NULL) && (top_element->checked == FALSE)){
            top_element->checked = TRUE;
            list_ins_next(&top_ten, NULL, top_element->word);
            top_ten.head->word_count = top_element->word_count;
            element = list->head;
            top_element = element;
            biggest = 0;
        }
    }
    
    //The following is implemented so that the count goes from 10-1
    count = list_size(&top_ten);
    for (element = list_head(&top_ten); element != NULL; element = element->next){
        printf("%d %s (%d)\n", count, element->word, element->word_count);
        count--;
    }
    list_destroy(&top_ten);
    
}

















