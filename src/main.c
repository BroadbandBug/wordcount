/**************************************************************************************************/


/*
 * File: main.c
 * Author: Roman Lysecky
 * Date: 11 August 2011
 *
 * Description: Template source file for main() function implementaiton for ECE 275 
 *              Assignment 1.
 *
 */

/**************************************************************************************************/


#include "global.h"
#include "list.h"
#include "wordrank.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>


/**************************************************************************************************/
int 
main(int argc, char *argv[]) 
{
    
    /* 
     * check for the correct number of commandline arguments. If incorrect
     * provide a simple usage message to the assit the user 
     */
	if( argc != 2 ) {
		printf("\nUsage: wordrank inputfile \n\n");
        return -1;
	}
    
    List word_list;
    
    list_init(&word_list);

    if(populate_list(argv[1],&word_list) == FALSE) return -1;
    
    if(list_size(&word_list) != 0){
        printf("Ten Most Frequently Used Words\n");
        top_words(&word_list, 10);
    }else{
        printf("ERROR: No words in file\n");
    }
    list_destroy(&word_list);
    
    
	return 0;
}	

/**************************************************************************************************/
