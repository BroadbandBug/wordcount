/**************************************************************************************************/
/*
 * File: list.c
 * Author: Kevin Klug
 * Date: 29 September 2011
 *
 * Description: Basic functions for the implementataion of linked lists
 *
 */
/**************************************************************************************************/


#include "global.h"
#include "list.h"
#include "wordrank.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

//initalizes the linked list specified by list.
void list_init(List *list){
    
    list->size = 0;
    list->head = NULL;
    list->tail = NULL;
    return;
    
}

//Destroys the linked list specified by list.
void list_destroy(List *list){
   
    while(list->size > 0){
        list_rem_next(list, NULL);
    }
    
    return;
}

//Inserts an element after the list element specified
bool list_ins_next( List *list, ListElmt *element, char *word){
    
    ListElmt *newElement;
    if( (newElement = (ListElmt *)malloc(sizeof(ListElmt))) == NULL ) return FALSE;
    
    //Make the element not checked and store the word
    newElement->checked = FALSE;
	
    char *new_word;
    
    new_word = (char *)malloc(strlen(word));
    strcpy(new_word, word);
    newElement->word = new_word;    
    
//If element pointer is null then the new element will be inserted at the head of the list
    if(element == NULL){
        newElement->next = list->head;
        list->head = newElement;
        
        //If the list is empty then
        if(list->size == 0){
            list->tail = newElement;
        }
    }else{
        
        //If the new element is at the end of the list then the tail will be changed to this location
        if(element->next == NULL){
            list->tail = newElement;
        }
        
        newElement->next = element->next;
        element->next = newElement;
        
    }
    
    list->size++;
    
    return TRUE;
    
}

//Removes the element subsequent to the element specified. If NULL pointer is sent as the next element the head will be removed
bool list_rem_next( List *list, ListElmt *element){
    
    ListElmt *old_element;
    
    //Cannot remove an element from an empty list
    if (list->size == 0) {
        return FALSE;
    }
    
    //Remove the element from the head of the list
    if (element == NULL) {
        
        old_element = list->head;
        list->head = list->head->next;
        
        //If there is only one element then the tail is also null
        if (list->size == 1){
            list->tail = NULL;
        }
    }else{
        
        old_element = element->next;
        element->next = element->next->next;
        
        if(element->next == NULL){
            list->tail = element;
        }
    }
    
    free(old_element->word);
    
    free(old_element);
    
    list->size--;
    return TRUE;
    
}

//Return the size of the list specified
int list_size(const List *list){
    return list->size;
}

//Return the location of the head of the list
ListElmt *list_head(List *list){
    return list->head;
}

//Return the location of the tail of the list
ListElmt *list_tail(List *list){
    return list->tail;
    
}
/**************************************************************************************************/
