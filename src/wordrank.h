/**************************************************************************************************/
/*
 * File: wordrank.h
 * Author: Kevin Klug
 * Date: 29 September 2011
 *
 * Description: Functions for the implementation of ECE275 Assigment 2: Wordrank
 *
 */
/**************************************************************************************************/

#include "global.h"

/**************************************************************************************************/

#ifndef WORDRANK_H
#define WORDRANK_H

//Populates the a linked list from a given file
bool populate_list( char *inputFileName, List *list);

//Given a list, this function seaches the list elements for the given string
ListElmt *repeated_word( List *list, char *input_word);

//Prints the top 10 words
void top_words(List *list, int num);

#endif

/**************************************************************************************************/

