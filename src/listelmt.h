/**************************************************************************************************/
/*
 * File: listelmt.h
 * Author: Kevin Klug
 * Date: 4 October 2011
 *
 * Description: Basic functions for the implementation of linked lists Elements
 *
 */
/**************************************************************************************************/
#include "global.h"


#ifndef Project_listelmt_h
#define Project_listelmt_h

//This is not included in list.h specific to the assignment's instructions
typedef struct ListElmt_ {
    char *word;
    int word_count;
    bool checked;
    struct ListElmt_ *next;
} ListElmt;

#endif
