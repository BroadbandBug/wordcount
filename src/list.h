/**************************************************************************************************/
/*
 * File: list.h
 * Author: Kevin Klug
 * Date: 29 September 2011
 *
 * Description: Basic functions for the implementation of linked lists
 *
 */
/**************************************************************************************************/

#include "global.h"
#include "listelmt.h"
/**************************************************************************************************/


#ifndef LIST_H
#define LIST_H


typedef struct List_ {
    int size;
    ListElmt *head;
    ListElmt *tail;
} List;

//Initalizes the linked list specified by list.
void list_init(List *list);

//Destroys the linked list specified by list.
void list_destroy(List *list);

//Inserts an element after the list element specified
bool list_ins_next( List *list, ListElmt *element, char *word);

//Removes the element subsequent to the element specified. If NULL pointer is sent as the next element the head will be removed
bool list_rem_next( List *list, ListElmt *element);

//Return the size of the list specified
int list_size(const List *list);

//Return the location of the head of the list
ListElmt *list_head(List *list);

//Return the location of the tail of the list
ListElmt *list_tail(List *list);

/**************************************************************************************************/

#endif

/**************************************************************************************************/

